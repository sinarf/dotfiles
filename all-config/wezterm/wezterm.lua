local wezterm = require("wezterm")
local config = wezterm.config_builder()

config.hide_tab_bar_if_only_one_tab = true

config.color_scheme = "Tokyo Night Storm"
-- config.color_scheme = "Dracula (Official)"
-- config.color_scheme = "Dracula"
config.window_background_opacity = 0.75
config.macos_window_background_blur = 60
-- config.window_background_image = os.getenv("HOME") .. "/Sync/pics/background/wp3365573.jpg"

config.window_background_image_hsb = {
    brightness = 0.1,
    hue = 1.0,
    saturation = 0.9,
}
config.font_size = 16.0
config.font = wezterm.font_with_fallback({
    "Cascadia Code",
    "JetBrainsMono Nerd Font",
    "FiraCode Nerd Font",
    "VictorMono Nerd Font",
    "CommitMono Nerd Font",
})

config.window_padding = {
    left = 20,
    right = 20,
    top = 20,
    bottom = 20,
}
config.initial_rows = 60
config.initial_cols = 225
config.adjust_window_size_when_changing_font_size = false

config.window_decorations = "RESIZE"

config.keys = {
    {
        key = "f",
        mods = "CTRL|CMD",
        action = wezterm.action.ToggleFullScreen,
    },
}
config.native_macos_fullscreen_mode = true

config.window_close_confirmation = "NeverPrompt"
return config
