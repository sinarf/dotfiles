#!/usr/bin/env bash
# this script is meant to initialize a new computer using arch based distribution to:
# * automatically install this dotfile repo with the needed dependencies
#
set -eu

export DOT_FILES="${HOME}/.dotfiles"
ssh_public_key_file="${HOME}/.ssh/id_ed25519.pub"

if command -v paru; then
  echo "paru is already ready, skipping installation..."
else
  echo "Installation of paru..."
  sudo pacman -S --needed --noconfirm base-devel rust
  sudo pacman -S --needed --noconfirm cargo
  tmp_paru_src_dir=$(mktemp -d)
  git clone https://aur.archlinux.org/paru.git "${tmp_paru_src_dir}"
  pushd "${tmp_paru_src_dir}" >/dev/null || exit 1
  makepkg -si
  popd >/dev/null || exit 1
  rm -rf "${tmp_paru_src_dir}"
fi

echo "Install packages pre-requisites..."
packages=(
  git
  stow
  zsh
  firefox
  tree
  kitty
  neovim
  trash-cli
)
paru -Syu --needed --noconfirm "${packages[@]}"

if [ -f "${ssh_public_key_file}" ]; then
  echo "ssh public key already created."
else
  echo "Generating a new ssh key"
  ssh-keygen -t ed25519
fi

echo "Register the public ssh key in git account"
firefox https://framagit.org/-/user_settings/ssh_keys
cat "${ssh_public_key_file}"
read -p "Confirm that this key is registered on git before continuing..." -n 1 -r

echo
echo "Setting up dotfiles in ${DOT_FILES}"
git clone git@framagit.org:sinarf/dotfiles.git "${DOT_FILES}"

export PATH=${DOT_FILES}/bin:${PATH}
echo "Moving files that might have been created in the installation process"

# Some files need to be removed to avoid conflicts
files_to_remove=("${HOME}/.bashrc" "${HOME}/.bash_profile" "${HOME}/.config/i3/config")
for file in "${files_to_remove[@]}"; do
  echo "Removing ${file}"
  if [ -f "${file}" ]; then
    trash --verbose --force "${file}"
  fi
done

update_dotfiles.sh

echo "Prefer dark theme for gtk applications"
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'

echo "Editing a few systems files as root..."
system_files=(
  /etc/pacman.conf
  /etc/lightdm/lightdm.conf
)
sudo nvim "${system_files[*]}"
sudo cp -v "${DOT_FILES}/init/00-keyboard.conf" /etc/X11/xorg.conf.d/
sudo usermod --shell /bin/zsh "${USER}"
sudo groupadd -r autologin || true
sudo gpasswd -a "${USER}" autologin
sudo gpasswd -a "${USER}" games

echo "Installing software..."
"${DOT_FILES}"/bin/batch_install_softwares.sh packages.lst
"${DOT_FILES}/init/install_custom_neovim_config.sh"
"${DOT_FILES}/init/install_bluetooth.sh"

echo "Might be a good time to reboot..."
