#!/usr/bin/env bash


if awesome -k; then
  notify-send --icon "info" "Awesome Configuration is awesome"
else
  notify-send --icon "error" "Awesome configuration is awful"
fi
