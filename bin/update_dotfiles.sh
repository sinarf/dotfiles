#!/usr/bin/env bash
# Update local dotfiles

set -eu

if [ -z "${DOT_FILES:-}" ]; then
  echo "The DOT_FILES environment variable not set!"
  echo "This message should only apply on the first run."
  dot_files=$(dirname "$0")/..
  export DOT_FILES=${dot_files}
fi

# We want those directory to exists so stow won't link the directory
# but the files individually allowing for customization I don't want to maintain
# Or because some scripts are expecting to have them
needed_dirs=(
  "$HOME"/bin
  "$HOME"/.local/bin
  "$HOME"/.local/share/applications
  "$HOME"/.config/autostart
  "$HOME"/.config/systemd/user
  "$HOME"/git/sinarf
  "$HOME"/git/work
  "$HOME"/tmp
  "$HOME"/Sync
)
for needed_dir in "${needed_dirs[@]}"; do
  mkdir -pv "$needed_dir"
done

pulled=false
echo "Moving to the dotfiles directory: ${DOT_FILES}"
cd "${DOT_FILES}"
echo "Pulling change from upstream..."
if git pull; then
  pulled=true
else
  echo "WARNING: pull failed, the code will NOT be pushed at the end of the script."
fi
git submodule update --init --recursive
echo "Linking dotfiles..."
stow -v1 --dotfiles all
stow -v1 --target="${HOME}"/.config all-config

if [[ "$(uname)" == Darwin ]]; then
  stow -v1 --dotfiles mac
  stow -v1 --target="${HOME}"/.config mac-config
else
  stow -v1 --dotfiles linux
  stow -v1 --target="${HOME}"/.config linux-config
  stow -v1 --target="${HOME}"/.local linux-local
  if systemctl --user status empty-trash | grep -q enabled; then
    echo "Empty trash service already enabled"
  else
    echo "Reload systemd..."
    systemctl --user daemon-reload
    systemctl --user status empty-trash
  fi
fi

if ${pulled}; then
  echo "Pushing local modifications..."
  git push
fi
