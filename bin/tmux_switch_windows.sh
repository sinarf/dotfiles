#!/usr/bin/env bash

selected_window_name="$(tmux lsw -F '#{window_name}' | sort | fzf)"

if [[ -n ${selected_window_name:-} ]]; then
  tmux select-window -t "$selected_window_name"
else
  tmux display-message "No Window selected"
fi
