#!/usr/bin/env bash

# This script create a tmux window for a specific project.
# It will be called by a tmux keymap.
# How it works:
# * Create a list of projects (mostly git repo)
#   --> The selected repo is a classic git repo or a simple directory.
#     --> Open a new tmux window with project name.
#   --> The selected repository is a bare repo, we need to choose or create a worktree
#     --> Select an existing worktree
#       --> Open a tmux window name by the project and the worktree.
#     --> The worktree does not exist yet (press Escape)
#       --> Select a branch and create the worktree.
#         --> Open a tmux window name by the project and the worktree.
#       --> No branch selected.
#         --> Quit without doing anything.

set -eu

# Check for dependencies.
if ! command -v tmux; then
  echo "ERROR: This script needs tmux."
fi
if ! command -v fzf; then
  echo "ERROR: This script needs fzf."
fi

git_projects_dir="$HOME/git"
if [[ ! -d $git_projects_dir ]]; then
  # Create the directory so it won't fail on a new machine, that does not have project yet.
  mkdir -pv "$git_projects_dir"
fi


open_tmux_window() {
  tmux new-window -n "${window_name}" -c "${project_dir}"
  exit 0
}

echo "Let's see what project are in $git_projects_dir..."
readarray -d '' git_repos < <(
  # Standard git repositories, created with a standard clone
  find "$git_projects_dir" -maxdepth 4 -type d -name .git -exec dirname {} \;
)
readarray -d '' git_worktrees < <(
  # Bare git repositories.
  # Those project need to be cloned with the script `./clone_for_worktree.sh`
  find "${git_projects_dir}" -maxdepth 4 -type d -name "*.git" -not -name '.git'
)
project_dirs=(
  "${git_worktrees[@]}"
  "${git_repos[@]}"
  # Some git repositories are not with other projects
  "$HOME/.dotfiles"
  "$HOME/.config/nvim"
  "$HOME/.config/kickstart-nvim"
  "$HOME/.config/lazyvim"
  "$HOME/Sync/pkb"
  "$HOME/.local/ms-mbl-tools"

)

# Select a project
project_dir=$(
  printf "%s\n" "${project_dirs[@]}" | fzf --prompt "Select project:" || exit 0
)
if [[ -n ${project_dir:-} ]]; then
  is_bare_repo=$(git -C "${project_dir}" rev-parse --is-bare-repository) || {
    # Some of the manually added repositories are not handle in git, like my Personal Knowledge base.
    echo "Not a git directory"
    window_name=$(basename "$project_dir")
    open_tmux_window
  }
  # When in bare repo, we need to choose a worktree.
  if $is_bare_repo; then
    worktree_dir=$(
      printf "%s\n" "$(git -C "${project_dir}" worktree list)" | fzf --prompt "Select Worktree [ESC to select a branch instead]:" | cut -d ' ' -f1
    )
    if [[ -z ${worktree_dir:-} ]]; then
      # If no worktree has been selected, we want to select a branch to create a new worktree.
      git -C "$project_dir" fetch
      selected_branch=$(
        git -C "$project_dir" branch -a --format='%(refname:short)' | fzf --prompt "Select Branch:" || exit 0
      )
      selected_branch=${selected_branch#*/}
      if [[ -n ${selected_branch:-} ]]; then
        worktree_dir="$project_dir/$selected_branch"
        if [[ ! -d ${worktree_dir:-} ]]; then
          # worktree does not exists, let's create it
          git -C "$project_dir" worktree add "$selected_branch" "$selected_branch"
        fi
      else
        # if no branch is selected at this time, we do nothing.
        echo "No worktree or branch selected, I quit!"
        echo "But, that OK, no breakage here, I understand you changed your mind, you can always ask again."
        exit 0
      fi
    fi
    window_name="$(basename "${project_dir%.*}")  $(basename "${worktree_dir}")"
    project_dir="${worktree_dir}"
  else
    # Non bare repo can be open as is.
    window_name=$(basename "${project_dir}" | sed 's/^\.//g')
  fi
  open_tmux_window
fi
