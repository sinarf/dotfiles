#!/usr/bin/env bash

server=${1}
if [[ $# -gt "1" ]]; then
  ssh "$@"
elif [ -z "${TMUX}" ]; then
  ssh -o RemoteCommand='bash -o vi' -o RequestTTY=yes "${server}"
else
  short_name=$(echo "${server}" | cut -f1 -d'.')
  tmux new-window -n "󰣀 $short_name" "ssh ${server} -t bash -o vi || {
                                                                                                        echo ''
                                                                                                        echo '  Will terminate momentarelly...'
                                                                                                        sleep 10
                                                                                                      }"
fi
