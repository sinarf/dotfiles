#!/usr/bin/env bash
# Personal adaptation of the script found here https://stackoverflow.com/a/62524752/257322

# This create a bare repo that fix allow for your worktrees to actually see the remote branches.
# There is a pretty good explanation in this answer: https://stackoverflow.com/a/54408181
#
# Dependencies:
#  * git
#  * trash-cli

set -e

url=$1
name=${url##*/}

git_dir="$HOME/git"
if [[ $url == *myscript* ]]; then
  git_dir="$git_dir/work"
else
  git_dir="$git_dir/sinarf"
fi

repo_dir=$git_dir/$name

if [[ -d $repo_dir ]]; then
  echo "ERROR: $repo_dir already exists!"
  exit 2
fi
echo "Initialisining git repo to work with worktrees"
mkdir -pv "$git_dir"
pushd "$git_dir" >/dev/null
git init --bare "${name}"
cd "${name}"
git config remote.origin.url "$url"
git config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'
git fetch

firstCommit=$(git rev-list --all --max-parents=0 --date-order --reverse | head -n1)
if [[ -z ${firstCommit} ]]; then
  echo "ERROR: Won't work with an empty repos, please create at least 1 commit it the repo and rerun this script."
  trash --verbose "$git_dir"
  exit 1
else
  git branch bare-dummy "$firstCommit"
  git symbolic-ref HEAD refs/heads/bare-dummy
fi

popd >/dev/null
