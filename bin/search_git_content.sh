#!/usr/bin/env bash

if [[ -z ${1:-} ]]; then
  echo "You need to provide a regex as the first parameter."
  exit 2
else
  regex=$1
fi

# shellcheck disable=SC2046  # we do want the split
git grep "$regex" $(git rev-list --all)
