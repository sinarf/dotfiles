[user]
    name = sinarf
    email = sinarf@sinarf.org

[github]
    user = sinarf

[init]
    defaultBranch = main

[alias]
    lg = log --word-diff  -p
    l = log --pretty='%C(yellow)%h\t %<(16)%Cblue%aN%C(auto)%d\t %Creset%s' -40
    lf = log --pretty='%C(yellow)%h\t %<(16)%Cblue%aN%C(auto)%d\t %Creset%s'
    ld = log --pretty='%C(yellow)%h\t %<(10)%C(cyan)%ci\t %<(16)%Cblue%aN%C(auto)%d\t %<(16)%Creset%s' -40
    ll = log --oneline --abbrev-commit --all --graph --decorate --color
    f = fetch
    st = status
    sub = submodule update --init --recursive
    unsub = submodule deinit --all
    hs = log --pretty='%C(yellow)%h %C(cyan)%cd %Cblue%aN%C(auto)%d %Creset%s' --graph --date=relative --date-order
    pr = fetch origin refs/pull-requests/*/from:refs/remotes/origin/pr/*
    cb = "! git rev-parse --abbrev-ref HEAD | yank -l1"
    tag-with-date = for-each-ref --sort=creatordate --format '%(refname)\t %(creatordate)' refs/tags

    ; never forget to fetch when rebasing on main branches
    riom = ! git fetch && git rebase -i origin/main
    riop = ! git fetch && git rebase -i origin/production

[includeIf "gitdir:~/git/work/"]
    path = ~/.gitconfig-work

[mergetool "nvim"]
    cmd = nvim -f -c \"Gdiffsplit!\" \"$MERGED\"

[merge]
    tool = nvim

[difftool]
    prompt = false

[push]
    default = simple
    autoSetupRemote = true

[core]
    ;excludesfile = ~/Sync/config/git/gitignore
    editor = nvim
    autocrlf = input

[log]
  date = relative
; [format]
;   pretty = format:%h %Cblue%ad%Creset %d %an %Cgreen%s%Creset

[url "https://"]
    insteadOf = git://

[svn]
    rmdir = true

[credential]
    helper = store


[pull]
    rebase = true

[add.interactive]
    useBuiltin = false # required for git 2.37.0

[rebase]
	autosquash = true

[color]
	diff = auto
	status = auto
	branch = auto
	interactive = auto
	ui = auto
	; pager = true
	grep = auto
	decorate = auto
	showbranch = auto
[color "diff"]
	meta = blue
	old = red strike
	new = green italic
	context = yellow bold
[color "branch"]
	local = green
	current = brightwhite yellow
	remote = normal blue
	upstream = brightwhite cyan
[color "fetch"]
	ui = auto
[fetch]
	prune = true
